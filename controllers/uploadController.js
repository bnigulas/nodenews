//failide üleslaadimise mõeldud kontroller
const Multer = require('multer');
const path = require('path');

//seadistame failide füüsilise salvestuse asukoha ja nime (andmebaasi paigutame ainult lingi)
const IMGstore = Multer.diskStorage({
    destination: function(req, file, next){
        //console.log('./public/img/' + file.fieldname +'/');
        next(null, path.resolve('./public/img/' + file.fieldname +'/'));
    },
    filename: function(req, file, next){
        next(null, Date.now() + '.' + file.originalname);
    }
});
let upload = Multer({storage: IMGstore});
module.exports = upload;
