//kontrolli, kas on sisse logitud, kui on jätka järgmise funktsiooniga, kui ei suuna sisselogimise lehele
exports.ensureAuthenticated = function(req,res,next){
    //passport annab funktsiooni, mis kontrollib on sisse logitus registreeritud
    if(req.isAuthenticated()){
        next()
    }
    else{
        //siinkohal saaks logida sõnumeid serverisse
        res.redirect('/users/login');
    }
}
// kui on sisselogitud, siis suuna dashboardi sellel lehelt (nt. sisselogimsivormilt)
exports.forwardAuthenticated = function(req, res, next){
    //kui ei oel sisselogitud, luba lehele, kui on sisse logitud, saada dashboardi
    if(!req.isAuthenticated()){
        next()
    }
    else{
        //siinkohal saaks logida sõnumeid serverisse
        res.redirect('/users/dashboard');
    }
}